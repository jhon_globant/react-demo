import React from 'react';
import ReactDOM from "react-dom";
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux';

import App from './components/AppComponent';
import { configure, history } from './config/configure-store';

const store = configure();

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App />
       </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
); 