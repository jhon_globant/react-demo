import React, { Component } from 'react';
import { Form,FormGroup,Col,FormControl,Button,Checkbox,Grid } from 'react-bootstrap';
import { Section } from './Components';


class Filter extends Component {

    render() {

        return (
            <Section>
            <Form horizontal>
            <FormGroup controlId="formHorizontalEmail">
              <Col sm={4}>
                Computer name:
              </Col>
              <Col sm={8}>
                <FormControl type="text" placeholder="" />
              </Col>
            </FormGroup>
        
            <FormGroup controlId="formHorizontalPassword">
              <Col sm={4}>
                Printer name:
              </Col>
              <Col sm={8}>
                <FormControl type="text" placeholder="" />
              </Col>
            </FormGroup>
        
            <FormGroup controlId="formHorizontalPassword">
              <Col sm={4}>
                Station:
              </Col>
              <Col sm={8}>
              <FormControl componentClass="select" placeholder="select">
              <option value="select">...</option>
              <option value="other">...</option>
            </FormControl>
              </Col>
            </FormGroup>
        
            <FormGroup controlId="formHorizontalPassword">
              <Col sm={4}>
                Location:
              </Col>
              <Col sm={8}>
              <FormControl componentClass="select" placeholder="select">
              <option value="select">DAL STORE</option>
              <option value="other">...</option>
            </FormControl>
              </Col>
            </FormGroup>
            <FormGroup>
            <Col sm={6}>
              <Checkbox inline>
                Active
              </Checkbox>
              {' '}
              <Checkbox inline>
                Inactivate
              </Checkbox>
              {' '}
              <Checkbox inline>
                All
              </Checkbox>
              </Col>
            </FormGroup>
          </Form>
          </Section>
        )
    }
}
export default Filter;
