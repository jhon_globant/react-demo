import React from 'react';
import styled from 'styled-components';

const Section = styled.section`
padding: 20px 5px 10px 20px;
border-style: solid;
border-width: 1px;
border-color:#8a8afd;
border-radius:10px;
`;

  export {Section};