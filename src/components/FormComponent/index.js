import React, { Component } from 'react';
import { Form,FormGroup,Col,FormControl,Button,Checkbox,Grid } from 'react-bootstrap';
import { Section } from './Components';


class FormStation extends Component {

    render() {

        return (
            <Section>
            <Form horizontal>
            <FormGroup controlId="formHorizontalEmail">
              <Col sm={4}>
                Printer name:
              </Col>
              <Col sm={7}>
                <FormControl type="text" placeholder="" />
              </Col>
            </FormGroup>
        
            <FormGroup controlId="formHorizontalPassword">
              <Col sm={4}>
                Computer name:
              </Col>
              <Col sm={7}>
                <FormControl type="text" placeholder="" />
              </Col>
            </FormGroup>
        
            <FormGroup controlId="formHorizontalPassword">
              <Col sm={4}>
                Station:
              </Col>
              <Col sm={7}>
              <FormControl componentClass="select" placeholder="select">
              <option value="select">...</option>
              <option value="other">...</option>
            </FormControl>
              </Col>
            </FormGroup>
        
            <FormGroup controlId="formHorizontalPassword">
              <Col sm={4}>
                Location:
              </Col>
              <Col sm={7}>
              <FormControl componentClass="select" placeholder="select">
              <option value="select">DAL STORE</option>
              <option value="other">...</option>
            </FormControl>
              </Col>
            </FormGroup>
            <FormGroup controlId="formHorizontalEmail">
              <Col sm={4}>
                Email:
              </Col>
              <Col sm={7}>
                <FormControl type="email" placeholder="" />
              </Col>
            </FormGroup>
            <FormGroup controlId="formHorizontalEmail">
              <Col md={1}>
              <Button bsStyle="primary" bsSize="small">Add</Button>
              </Col>
              <Col md={11}>
              <Button bsStyle="primary" bsSize="small">Clear</Button>
              </Col>
            </FormGroup>
          </Form>
          </Section>
        )
    }
}
export default FormStation;