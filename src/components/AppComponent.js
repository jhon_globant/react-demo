import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import { MainRoute } from './../routes/MainRoutes';
import styled from 'styled-components';
import  Header  from '../components/HeaderComponent/index';
const Container = styled.section`
    width: 100%;
    display: block;
`;

const Content = styled.section`
    width: 100%;
    display: block;
    overflow-x: hidden;
    padding: 10px 30px 0px 30px;
`;



export default class AppComponent extends React.Component {

    render() {
        return (
            <Container>
                <Header/>
                <Content>
                    {MainRoute()}
                </Content>
            </Container>
        );
    }


}
