import React, { Component } from 'react';
import { Nav,Navlogo,Menu,MenuOption } from './components';
import { Switch, Route, Link } from 'react-router-dom';
import Icon from 'svg-react-loader?name=Icon!../../assets/img/swa_logo_light.svg';

class Header extends Component {

    render() {
        return (
        <Nav>

            <Menu>
                <MenuOption><Icon className='normal' width='165px' /></MenuOption>
                <MenuOption><a href="/">Home</a></MenuOption>
                <MenuOption><a href="/others">Others</a></MenuOption>
            </Menu>

        </Nav>
        
        );
    }
}

export default Header;