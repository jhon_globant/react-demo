import React from 'react';
import styled from 'styled-components';

const Nav = styled.div`
  overflow: hidden;
  background-color:#304CAF;
`;
const Navlogo = styled.img`
  width:0;
  height: 25px;
  flex-grow: 0.2;
  flex: 0.5 0.5px;
  flex-basis: 1px;
`;

const Menu = styled.ul`
    
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color:#304CAF;
}
`;

const MenuOption = styled.li`
    float: left;

    > a {
        display: block;
        color: white;
        text-align: center;
        padding: 14px 16px;
        border-right: #304CAF 2px solid;
        text-decoration: none;
      }

    > a:hover {
        background-color: #2683f9;
    }

    > svg {
      padding: 10px;
    }
`;
  export {Nav,Navlogo,Menu,MenuOption};