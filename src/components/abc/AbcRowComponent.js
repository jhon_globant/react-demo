import React from 'react';

import { RIEInput } from 'riek';

export default class AbcRowComponent extends React.Component {

    constructor(props) {
        super(props);
        let { item, remove } = this.props;
        this.state = {
            x: item.x,
            y: item.y,
            z: item.z,
            isEditable: true,
            isDisabled: false,
        };
    }

    render() {
        return (
            <tr>
                <td>
                    <RIEInput
                        value={this.state.x}
                        change={ (value) => { this.setState({ x: value.text}); }}
                        propName="text"
                        className={this.state.isEditable ? "editable" : ""}
                        classLoading="loading"
                        classInvalid="invalid"
                        isDisabled={this.state.isDisabled} />
                </td>
                <td>
                    <RIEInput
                        value={this.state.y}
                        change={ (value) => { this.setState({ y: value.text}); }}
                        propName="text"
                        className={this.state.isEditable ? "editable" : ""}
                        classLoading="loading"
                        classInvalid="invalid"
                        isDisabled={this.state.isDisabled} />
                </td>
                <td>
                    <RIEInput
                        value={this.state.z}
                        change={ (value) => { this.setState({ z: value.text}); }}
                        propName="text"
                        className={this.state.isEditable ? "editable" : ""}
                        classLoading="loading"
                        classInvalid="invalid"
                        isDisabled={this.state.isDisabled} />
                </td>
                <td>
                    <button>
                        Edit
                    </button>
                    <button>
                        Delete
                    </button>
                </td>
            </tr>
        );
    }
}
