import React from 'react';
import axios from 'axios';
import Title from './../TitleComponent';
import AbcForm from './AbcFormComponent';
import AbcList from './AbcListComponent';


export default class AbcAppComponent extends React.Component {
    render() {
        return (
            <div>
                <Title  abcCount={this.props.data.length} />
                <AbcForm addAbc={this.props.actions.addAbc.bind(this)} />
                <AbcList data={this.props.data} />
            </div>
        );
    }
}
