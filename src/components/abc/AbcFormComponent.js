import React from 'react';
import styled from 'styled-components';

const TodoForm = styled.section`
  padding: 1em;
  background: #eeeeeeba;
`;

const DivForm = styled.div`
    padding: 5px;
`;

const InputForm = styled.input`
    width: 96%;
`;

const ButtonForm = styled.button`
    padding: 5px;
`;

const ButtonContainer = styled.div`
    text-align: center;
`;

let input;

export default class TodoFormComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            x: '',
            y: '',
            z: ''
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const id = event.target.id;
        const value = event.target.value;
        switch (id) {
            case 'x':
                this.setState({ x: value });
                break;
            case 'y':
                this.setState({ y: value });
                break;
            case 'z':
                this.setState({ z: value });
                break;
        }
    }

    handleSubmit() {
        this.props.addAbc([this.state.x,this.state.y,this.state.z]);
    }
    
    render() {
        return (
            <TodoForm>
                <DivForm>
                   X Field ({this.state.x}): <InputForm id="x" value={this.state.x} onChange={this.handleChange} placeholder="Type your x value" />
                </DivForm>
                <DivForm>
                    Y Field ({this.state.y}): <InputForm id="y" value={this.state.y} onChange={this.handleChange} placeholder="Type your y value" />
                </DivForm>
                <DivForm>
                    Z Field ({this.state.z}): <InputForm id="z" value={this.state.z} onChange={this.handleChange} placeholder="Type your z value" />
                </DivForm>

                <ButtonContainer>
                    <ButtonForm onClick={this.handleSubmit.bind(this)}> Save </ButtonForm>
                </ButtonContainer>
            </TodoForm>
        );
    }
}
