import React from 'react';
import styled from 'styled-components';
import AbcRow from './AbcRowComponent';

const ListContainer = styled.div`
  padding: 1em;
  background: #eeeeeeba;
`;

const Table = styled.table`
    width: 100%;
`;

const TableTitle = styled.th`
    background: #4163e0e0;
    color: white;
`;


export default class AbcListComponent extends React.Component {

    dataList() {
        let { data, remove } = this.props;
        return data.map((item) => {
            return (<AbcRow item={item} key={item.id} remove={remove} />)
        });
    }

    render() {
        return (
            <ListContainer>
                <Table>
                    <thead>
                        <tr>
                            <TableTitle>X</TableTitle>
                            <TableTitle>Y</TableTitle>
                            <TableTitle>Z</TableTitle>
                            <TableTitle>Actions</TableTitle>
                        </tr>
                    </thead>
                    <tbody>
                        {this.dataList()}
                    </tbody>
                </Table>
            </ListContainer>
        );
    }
}