import React from 'react';
import styled from 'styled-components';

const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: #304cb2;
`;

export default class TitleComponent extends React.Component {
    render() {
        return (
            <div>
                <Title>Pioneer POD - ABC CRUD! ({this.props.abcCount}) </Title>
            </div>
        );
    }
}
