import React, { Component } from 'react';
import { Col, Table, Grid, Button } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { SectionTable } from './Components';

class List extends Component {

    
     onAfterSaveCell(row, cellName, cellValue) {
        alert(`Save cell ${cellName} with value ${cellValue}`);
      
        let rowStr = '';
        for (const prop in row) {
          rowStr += prop + ': ' + row[prop] + '\n';
        }
      
        alert('Thw whole row :\n' + rowStr);
      }
      
       onBeforeSaveCell(row, cellName, cellValue) {
        // You can do any validation on here for editing value,
        // return false for reject the editing
        return true;
      }

    render() {
        var products = [
            {
                printer: "//PRINT1",
                computerName: "PC1",
                station: "HOU",
                location: "LOUSTORE",
                email: "tracy@wnco.com"
            },
            {
                printer: "//PRINT1",
                computerName: "PC1",
                station: "HOU",
                location: "LOUSTORE",
                email: "tracy@wnco.com"
            },
            {
                printer: "//PRINT1",
                computerName: "PC1",
                station: "HOU",
                location: "LOUSTORE",
                email: "tracy@wnco.com"
            },
            {
                printer: "//PRINT1",
                computerName: "PC1",
                station: "HOU",
                location: "LOUSTORE",
                email: "tracy@wnco.com"
            },
            {
                printer: "//PRINT1",
                computerName: "PC1",
                station: "HOU",
                location: "LOUSTORE",
                email: "tracy@wnco.com"
            },
            {
                printer: "//PRINT1",
                computerName: "PC1",
                station: "HOU",
                location: "LOUSTORE",
                email: "tracy@wnco.com"
            },
            {
                printer: "//PRINT1",
                computerName: "PC1",
                station: "HOU",
                location: "LOUSTORE",
                email: "tracy@wnco.com"
            },
            {
                printer: "//PRINT1",
                computerName: "PC1",
                station: "HOU",
                location: "LOUSTORE",
                email: "tracy@wnco.com"
            },
            {
                printer: "//PRINT1",
                computerName: "PC1",
                station: "HOU",
                location: "LOUSTORE",
                email: "tracy@wnco.com"
            },
            {
                printer: "//PRINT1",
                computerName: "PC1",
                station: "HOU",
                location: "LOUSTORE",
                email: "tracy@wnco.com"
            },
            {
                printer: "//PRINT1",
                computerName: "PC1",
                station: "HOU",
                location: "LOUSTORE",
                email: "tracy@wnco.com"
            },
        ];

        const options = {
            page: 1,  // which page you want to show as default
            sizePerPageList: [{
                text: '10', value: 10
            }], // you can change the dropdown list for size per page
            sizePerPage: 10,  // which size per page you want to locate as default
            pageStartIndex: 1, // where to start counting the pages
            paginationSize: 3,  // the pagination bar size.
            prePage: 'Prev', // Previous page button text
            nextPage: 'Next', // Next page button text
            firstPage: 'First', // First page button text
            lastPage: 'Last', // Last page button text
            paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
            paginationPosition: 'bottom',  // default is bottom, top and both is all available
            // hideSizePerPage: true > You can hide the dropdown for sizePerPage
            // alwaysShowAllBtns: true // Always show next and previous button
            // withFirstAndLast: false > Hide the going to First and Last page button
        };

        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            beforeSaveCell: this.onBeforeSaveCell, // a hook for before saving cell
            afterSaveCell: this.onAfterSaveCell  // a hook for after saving cell
        };

        return (
            <SectionTable>
                <BootstrapTable
                    data={products}
                    version='4'
                    pagination={true}
                    options={options}
                    cellEdit={cellEditProp}
                >
                    <TableHeaderColumn isKey={true} dataField='printer'>Printer</TableHeaderColumn>
                    <TableHeaderColumn dataField='computerName'>Computer Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='station'>Station</TableHeaderColumn>
                    <TableHeaderColumn dataField='location'>Location</TableHeaderColumn>
                    <TableHeaderColumn dataField='email'>Email</TableHeaderColumn>
                </BootstrapTable>
                <Button bsStyle="primary" bsSize="small">Save changes</Button>
            </SectionTable>
        )
    }
}
export default List;