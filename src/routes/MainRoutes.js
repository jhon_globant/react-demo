import React from 'react';
import { Switch, Route } from 'react-router-dom';
import AbcApp from './../containers/AbcAppContainer';
import Title from './../components/TitleComponent';
import HomeComponent, { Home } from '../containers/Home/HomeComponent';

export const MainRoute = function(){
    return (
        <Switch>
            <Route exact path="/" component={HomeComponent} />
            <Route path="/others" component={AbcApp} />
        </Switch>
    );
}