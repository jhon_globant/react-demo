import { combineReducers } from 'redux'
import { items, itemsHasErrored, itemsIsLoading } from './AbcReducer'

const reducers = combineReducers({
  items,
  itemsHasErrored,
  itemsIsLoading
})

export default reducers;