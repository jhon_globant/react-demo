import  Filter  from '../../components/FilterComponent/index'
import  FormStation  from '../../components/FormComponent/index'
import  List  from '../../components/ListComponent/index'
import React, { Component } from 'react';
import { Col,Row,Container,Grid } from 'react-bootstrap';
class Home extends Component {


    render() {

        return (
                <div>
                    <Row>
                        <Col sm={6}>
                            <Filter/>
                        </Col>
                        <Col sm={6}>
                            <FormStation/>
                        </Col>
                    </Row>
                    <List/>
                </div>
          
        )
    }
}
export default Home;