import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AbcAppComponent from '../components/abc/AbcAppComponent';
import * as abcActions from '../actions/AbcActions';

class AbcAppContainer extends React.Component {

    render() {
        return (
            <AbcAppComponent 
                data={this.props.items}
                actions={this.props.actions}  />
        )
    }
    
    componentDidMount() {
        this.props.actions.getAbcList();
    }

}

function mapStateToProps(state, ownProps) {
    return { 
        items: state.items 
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(abcActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AbcAppContainer);